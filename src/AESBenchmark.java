
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * Code for benchmarking and timing AES encryption throughput and keytest rates.
 * Iteration counts are set so the total execution time is 10-15 seconds on an
 * Intel i7-3770 (3.4 GHz).
 *
 * @author srtate
 */
public class AESBenchmark {
    /**
     * Test how many different keys can be tested per second in a brute force
     * attack. Keys are basically treated as a big binary number and incremented
     * between each test. This guarantees that any previous key schedule setup
     * work cannot be reused.
     */
    public static void testKeyRate() {
        try {
            byte[] key = new byte[16];
            byte[] data = new byte[16];
            byte[] out = null;
            final int NUMTESTS = 6000000;

            Cipher cipher = Cipher.getInstance("AES");
            for (int k = 0; k < NUMTESTS; k++) {
                cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(key, "AES"));
                out = cipher.doFinal(data);

                int i = 0;
                while (++key[i] == 0) {
                    i++;
                }
            }
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(AESBenchmark.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * This is a fairly tight loop that encrypts a megabyte at a time, using a
     * single key. Returns the byte array - may not be necessary, but by
     * returning it we ensure that the optimizer doesn't remove the computation
     * because the array is never used.
     * 
     * @return the byte array of ciphertext
     */
    public static byte[] testThroughput() {
        final int TESTITER = 6000;
        byte[] out = null;
        try {
            byte[] data = new byte[1024 * 1024];  // 1 MB
            Cipher cipher = Cipher.getInstance("AES");
            cipher.init(Cipher.ENCRYPT_MODE, new SecretKeySpec(new byte[16], "AES"));
            for (int i = 0; i < TESTITER - 1; i++) {
                out = cipher.update(data);
                data = out;
            }
            out = cipher.doFinal(data);
        } catch (NoSuchAlgorithmException | NoSuchPaddingException | InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(AESBenchmark.class.getName()).log(Level.SEVERE, null, ex);
        }
        return out;
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        testThroughput();
//        testKeyRate();
    }

}
