

import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyAgreement;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

/**
 * Chat conversation for CSC 580 chat program. Objects in this class represent
 * active chat conversations, and interact with the chat hub for communication
 * and a ChatView object to initiate/report chat actions. Note that this class
 * must be initialized with the setChatViewController static method before any
 * Conversation objects can be created.
 * 
 * @version 0.1
 * @author srtate
 */
public class Conversation {
    public final static int STATUS_NOTCONN = 0;
    public final static int STATUS_CONN = 1;

    // One chat view controller for all conversations
    private static ChatViewController cvc;
    
    protected int id;
    protected int status;
    protected final String otherID;
    protected final HubSession hubConn;
    protected final ChatView view;

    Cipher cipher;
    SecretKeySpec keySpec;
    Base64.Decoder b64decoder;
    Base64.Encoder b64encoder;
    
    KeyAgreement keyAgree;
    KeyPairGenerator kpg;
    KeyPair kp;
    byte[] secret=null;

    private void initCrypto() {
        try {
            cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
            byte[] keybytes = kp.getPublic().getEncoded();
            keySpec = new SecretKeySpec(keybytes, "AES");
            b64decoder = Base64.getDecoder();
            b64encoder = Base64.getEncoder();
        } catch (NoSuchAlgorithmException | NoSuchPaddingException ex) {
            Logger.getLogger(Conversation.class.getName()).log(Level.SEVERE, null, ex);
        }
    }


    /**
     * Initialize Conversation class with an object that allows for views to
     * be created appropriate to the application.
     * @param cvc_in
     */
    public static void setChatViewController(ChatViewController cvc_in) {
        cvc = cvc_in;
    }
    
    /**
     * Create a new conversation for an already-established conversation (one
     * that already has a session id from the chat hub).
     * @param hub the logged-in chat hub session
     * @param sessionID the already-established session id
     * @param connTo the username on the other end of the connection
     */
    public Conversation(HubSession hub, int sessionID, String connTo) {
        status = STATUS_CONN;
        id = sessionID;
        otherID = connTo;
        hubConn = hub;
        view = cvc.newView(this);
        establish(id);
        initCrypto();
    }

    /**
     * Create a new conversation object for a conversation that hasn't been
     * connected yet. This is created when the user makes a request for a new
     * chat conversation, but when the connection hasn't been acknowledged (and
     * a session id assigned) by the chat hub. Before this can be used, the
     * connection must be completed, and establish() must be called.
     * @param hub the logged-in chat hub session
     * @param sessionID the already-established session id
     * @param connTo the username on the other end of the connection
     */
    public Conversation(HubSession hub, String connTo) {
        status = STATUS_NOTCONN;
        hubConn = hub;
        otherID = connTo;
        view = cvc.newView(this);
        hub.connectRequest(this);
        initCrypto();
    }

    /**
     * Establish a session. This is called when a connection request is
     * completed and acknowledged by the chat hub.
     * @param id the session ID for this conversation
     */
    public void establish(int id) {
        this.id = id;
        this.status = STATUS_CONN;
        view.setConnStatus(true);
        view.addInfoMessage("*** CONNECTED to " + otherID);
    }

    /**
     * Call this when a connection attempt has been made, creating the
     * Conversation object, but the connection does not go through.
     */
    public void failed() {
        view.addInfoMessage("*** CONNECT ATTEMPT FAILED");
        view.disconnectConvo();
    }

    /**
     * Drop an active connection. Conversation object can't be use do send
     * or receive messages after this, unless a new conversation is established
     * through a call to establish().
     */
    public void drop() {
        if (STATUS_CONN == status) {
            hubConn.dropConvo(id);
            this.id = -1;
            this.status = STATUS_NOTCONN;
            view.setConnStatus(false);
            view.addInfoMessage("*** DISCONNECTED");
        }
    }

    /**
     * Get the identity of the other side of the chat conversation.
     * @return the other user name
     */
    public String getOtherID() {
        return otherID;
    }

    private String myEncrypt(String plaintext) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        String returnValue = null;
        int index=0;
        if(plaintext.charAt(0)==':'){
            for(int i=0;i>plaintext.length();i++){
                if(plaintext.charAt(i)==' '){
                    index=i;
                }
            }
            String subStr=plaintext.substring(1, index-1);
            
            switch (subStr){
                case "kaok":
                    
                    kpg=KeyPairGenerator.getInstance("EC");
                    AlgorithmParameterSpec retValue = new ECGenParameterSpec("secp224r1");
                    kpg.initialize(retValue);
                    kp=kpg.generateKeyPair();
                   
                    
                    byte[] pubKey=kp.getPublic().getEncoded();
                    plaintext=Arrays.toString(pubKey);
                    break;

                case "err":
                    return "Decryption Failed";
                    
                case "fail":
                    return "Non-recoverable error";
                    
                            
            }
        }
        
        try {
            cipher.init(Cipher.ENCRYPT_MODE, keySpec);
            byte[] ctext = cipher.doFinal(plaintext.getBytes());
            byte[] iv = cipher.getIV();
            byte[] combined = new byte[iv.length + ctext.length];
            System.arraycopy(iv, 0, combined, 0, iv.length);
            System.arraycopy(ctext, 0, combined, iv.length, ctext.length);
            returnValue = b64encoder.encodeToString(combined);
        } catch (InvalidKeyException | IllegalBlockSizeException | BadPaddingException ex) {
            Logger.getLogger(Conversation.class.getName()).log(Level.SEVERE, null, ex);
        }
        return returnValue;
    }

    private String myDecrypt(String ciphertext) throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException {
        String returnValue = null;
        int index=0;
        try {
            byte[] binaryCiphertext = b64decoder.decode(ciphertext);
            if (binaryCiphertext.length < 32) {
                return null;
            }
            AlgorithmParameterSpec ivSpec = new IvParameterSpec(binaryCiphertext, 0, 16);
            cipher.init(Cipher.DECRYPT_MODE, keySpec, ivSpec);
            returnValue = new String(cipher.doFinal(binaryCiphertext, 16, binaryCiphertext.length - 16));
        } catch (IllegalArgumentException | IllegalBlockSizeException | BadPaddingException | InvalidKeyException | InvalidAlgorithmParameterException ex) {
            Logger.getLogger(Conversation.class.getName()).log(Level.SEVERE, null, ex);
        }
        if(returnValue.charAt(0)==':'){
            for(int i=0;i>returnValue.length();i++){
                if(returnValue.charAt(i)==' '){
                    index=i;
                }
            }}
        
            String subStr=returnValue.substring(1, index-1);
            

            
            switch (subStr){
                case "ka1":
                    String subStr2=returnValue.substring(index+1, returnValue.length());
                    byte[] encodedKey=subStr2.getBytes();
                    X509EncodedKeySpec keySpec = new X509EncodedKeySpec(encodedKey);
                    KeyFactory keyFac = KeyFactory.getInstance("RSA");
                    PublicKey publicKey = keyFac.generatePublic(keySpec);
                    
                    keyAgree= KeyAgreement.getInstance("ECDH");
                    keyAgree.init(kp.getPrivate());
                    keyAgree.doPhase(publicKey, true);
                    
                    byte[] secretGen=keyAgree.generateSecret();
                    System.arraycopy(secretGen,secretGen.length-16,secret,0,16);
                    break;
            }
        
        return returnValue;
    }
    
    /**
     * Process a message that has been received as part of a conversation.
     * Basically just sends the message on to the view so it can be displayed
     * or processed.
     * 
     * TODO: Make this into a secure message receiver, which will have to
     * decrypt the message before sending the recovered plaintext to the view.
     * 
     * @param message the message that was received
     */
    public void received(String ctext) throws NoSuchAlgorithmException, InvalidKeyException, InvalidKeySpecException {
        String plaintext = myDecrypt(ctext);
        if (plaintext != null) {
            view.addReceivedMessage(this, plaintext);
        } else {
            view.addReceivedMessage(this, "*** Bad Ciphertext: "+ctext);
        }
    }

    /**
     * Send a message on an established conversation. Tells the hub to actually
     * transmit the message.
     * 
     * TODO: Make this into a secure message sender, which encrypts the plaintext
     * message before sending it to the hub.
     * 
     * @param message the message to send
     */
    public void sendMessage(String message) throws NoSuchAlgorithmException, InvalidAlgorithmParameterException {
        if (STATUS_CONN == status) {
            hubConn.sendMessage(id, myEncrypt(message));
        }
    }
}
