/*
 * For UNCG CSC 580 - Spring 2017 - Project, Phase 3 - Benchmarking PK algs
 */

import java.io.IOException;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.spec.AlgorithmParameterSpec;
import java.security.spec.ECGenParameterSpec;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.RSAKeyGenParameterSpec;
import java.security.spec.X509EncodedKeySpec;
import java.util.Arrays;
import java.util.Base64;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.KeyAgreement;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SealedObject;
import javax.crypto.spec.DHParameterSpec;

/**
 * This class contains skeleton code to fill in for benchmarking several public
 * key crypto techniques, including RSA, Diffie-Hellman, and Elliptic Curve
 * Diffie-Hellman. Precomputed parameters and public keys are available for the
 * two Diffie-Hellman variants.
 * 
 * @author Steve Tate
 */
public class PKBenchmark {
    private static final String b64_dhParams = "MIICDQKCAQEAg3WAFz2NwnXa2HvrO5"
          + "2p/yKg1Kix8iTkMOV/dBlpHiIaJ8wJ+4PBcjayfxC8iWxEe35b/8+5/7l4psPS1Z9"
          + "dC4d47lfi8F/vAOfbg1hxBPBKzqNEInN97OKJYSbVFSAaXQImwWimf7miSmiDisdD"
          + "puS5XDfGfBW6JzPrE+VGUt9x6iakp5DjbgE8AWBifx6Ef8uyT8mq+Ku2O9ySlA6x4"
          + "oGB5/nVtvx8GaYhQqSPSeeyUS74FCzu0cIeB7/9CD1sfJLzbMNfvxmrCqlqFpvzsk"
          + "nLwG/tEUH5acyXMRplfYauLTBIov1V2sJABrepjyKClDaZvzjcNidLMPrWcWJttQK"
          + "CAQAZqs3u7bGDprnJI3WvwzcYgMRphPJ73kTSA7qAc/7OabQNt6XgckImJDEi77rW"
          + "M4WgvAUGn6UHv/HHnQQ57hLq5KPGuGe8sBYARPIMc5WxThrAf+9ftIFPKJs5njOjh"
          + "4h6IKp6G9PCHZTxxCjg86qOW5kZ7lTef/cvwwvCsMT3/0jJkM3YBsNZT1cU5WINxY"
          + "r9PPyb3LigYYKwc6px2Oqo1+I5OGVvxnDU/yaMY0Xt9+RPJxnZHMWy2JLokExqgkv"
          + "WGUeIZCvQZdwN97B9sNNVeWvbkfAJbeXafEb3Ey+uKyB113zX54LkxigfB+zZWVi3"
          + "dtDr0xwdqLMEpDBrjPDIAgIH/w==";

    private static final String B64_BOBPK_DH = "MIIDKTCCAhwGCSqGSIb3DQEDATCCAg"
          + "0CggEBAIN1gBc9jcJ12th76zudqf8ioNSosfIk5DDlf3QZaR4iGifMCfuDwXI2sn8"
          + "QvIlsRHt+W//Puf+5eKbD0tWfXQuHeO5X4vBf7wDn24NYcQTwSs6jRCJzfeziiWEm"
          + "1RUgGl0CJsFopn+5okpog4rHQ6bkuVw3xnwVuicz6xPlRlLfceompKeQ424BPAFgY"
          + "n8ehH/Lsk/JqvirtjvckpQOseKBgef51bb8fBmmIUKkj0nnslEu+BQs7tHCHge//Q"
          + "g9bHyS82zDX78Zqwqpahab87JJy8Bv7RFB+WnMlzEaZX2Gri0wSKL9VdrCQAa3qY8"
          + "igpQ2mb843DYnSzD61nFibbUCggEAGarN7u2xg6a5ySN1r8M3GIDEaYTye95E0gO6"
          + "gHP+zmm0Dbel4HJCJiQxIu+61jOFoLwFBp+lB7/xx50EOe4S6uSjxrhnvLAWAETyD"
          + "HOVsU4awH/vX7SBTyibOZ4zo4eIeiCqehvTwh2U8cQo4POqjluZGe5U3n/3L8MLwr"
          + "DE9/9IyZDN2AbDWU9XFOViDcWK/Tz8m9y4oGGCsHOqcdjqqNfiOThlb8Zw1P8mjGN"
          + "F7ffkTycZ2RzFstiS6JBMaoJL1hlHiGQr0GXcDfewfbDTVXlr25HwCW3l2nxG9xMv"
          + "risgddd81+eC5MYoHwfs2VlYt3bQ69McHaizBKQwa4zwyAICB/8DggEFAAKCAQAzo"
          + "BXZ3RfUSfe9tkpedufUq7dJRcmu5tDib+khtgPQt2aC2QpL9RPRB2DkEzerdJ4E87"
          + "M6vyKrwAkbQA0OQvQRgLftHCMl6ybguxXR29zxJN+3n8wPg/kYWsfYAjxtOzcULIk"
          + "N5Gwj2vgW9MfNk6PZZb+SphLpcUAkfMjChgPLkfRSd/ePCgxY2fd4+axHWrzdc/cf"
          + "E3D0vdXsJq4rqxuHxkcvWi+87UVGpuZayiNE5+dBJX9Grf8ZzwLZ7NuW9R9FfJjq6"
          + "h7gZeY6huC2YmCTzkC4wiCfNjCRzjmMwrdrpG2hW89H3ncNxJvvZywXrKtrqRngvP"
          + "cB+mlfmobPGiEf";

    private static final String B64_BOBPK_ECDH = "ME4wEAYHKoZIzj0CAQYFK4EEACED"
          + "OgAEr9k0PjIrH8CSFiHsA2GFZlnOPWSfrk58k8xic0ENK8KdwpnTy8j9RAGKEe2fy"
          + "5DatB08sWL878g=";

    /**
     * Unpacks a public key from a base64 encoded string, and returns it as
     * a PublicKey object.
     * 
     * @param alg the key style/algorithm ("DiffieHellman" or "EC")
     * @param pk_b64 the base64 encoded public key
     * @return the PublicKey
     */
    public static PublicKey getBobPublic(String alg, String pk_b64) {
        PublicKey retKey = null;
        try {
            byte[] encPubKey = Base64.getDecoder().decode(pk_b64);
            KeyFactory kf = KeyFactory.getInstance(alg);
            retKey = kf.generatePublic(new X509EncodedKeySpec(encPubKey));
        } catch (NoSuchAlgorithmException | InvalidKeySpecException ex) {
            Logger.getLogger(PKBenchmark.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retKey;
    }

    /**
     * Loads in Diffie-Hellman parameters from the fixed modulus/generator
     * parameter encoded above. The resulting AlgorithmParameterSpec can be used
     * to initialize a KeyPairGenerator before using genKeyPair().
     * 
     * @return the AlgorithmParamSpec needed for generating DH keypairs
     */
    public static AlgorithmParameterSpec getDHParameterSpec() {
        AlgorithmParameterSpec retValue = null;
        try {
            AlgorithmParameters params = AlgorithmParameters.getInstance("DiffieHellman");
            params.init(Base64.getDecoder().decode(b64_dhParams));
            retValue = params.getParameterSpec(DHParameterSpec.class);
        } catch (NoSuchAlgorithmException | IOException | InvalidParameterSpecException ex) {
            Logger.getLogger(PKBenchmark.class.getName()).log(Level.SEVERE, null, ex);
        }
        return retValue;
    }

    /**
     * Loads in Elliptic Curve Diffie-Hellman parameters from the built-in
     * standard curve named "secp224r1" - this is the JCA name for the
     * NIST P-224 curve.
     * 
     * @return the AlgorithmParamSpec needed for generating ECDH keypairs
     */
    public static AlgorithmParameterSpec getECDHParameterSpec() {
        AlgorithmParameterSpec retValue = new ECGenParameterSpec("secp224r1");
        return retValue;
    }

    /**
     * Test Diffie-Hellman timing. This should repeatedly do Diffie-Hellman
     * key agreement calculations, as many times as are necessary so that it runs
     * approximately 10 seconds (so you get accurate timings).
     * 
     * @param bobPK the public key for the "other end" of the key agreement
     */
    public static void testDH(PublicKey bobPK) throws NoSuchAlgorithmException, InvalidKeyException, InvalidAlgorithmParameterException {
        // YOUR CODE GOES HERE
        final int NUMTESTS=2500000;
        byte[] test;
        KeyPairGenerator kpg=KeyPairGenerator.getInstance("DH");
        kpg.initialize(getDHParameterSpec());
        KeyPair kp=kpg.genKeyPair();
        
        KeyAgreement ka= KeyAgreement.getInstance("DiffieHellman");
        

        
        
        for(int i=0;i<NUMTESTS;i++){
            ka.init(kp.getPrivate(),getDHParameterSpec());
            ka.doPhase(bobPK, true);
            //test=ka.generateSecret();
        }
        
    }
    
    /**
     * Test Elliptic Curve Diffie-Hellman timing. This should repeatedly do ECDH
     * key agreement calculations, as many times as are necessary so that it runs
     * approximately 10 seconds (so you get accurate timings).
     * 
     * @param bobPK the public key for the "other end" of the key agreement
     */
    public static void testECDH(PublicKey bobPK) throws InvalidKeyException, InvalidAlgorithmParameterException, NoSuchAlgorithmException, IllegalStateException {
        // YOUR CODE GOES HERE
        final int NUMTESTS=550000000;
        KeyPairGenerator kpg=KeyPairGenerator.getInstance("EC");
        kpg.initialize(getECDHParameterSpec());
        KeyPair kp=kpg.genKeyPair();
        
        KeyAgreement ka= KeyAgreement.getInstance("ECDH");
        
        
        for(int i=0;i<NUMTESTS;i++){
            ka.init(kp.getPrivate());
        
            ka.doPhase(bobPK, true);
            //test=ka.generateSecret();
        }
        
        
        
    }
    
    /**
     * Generates an RSA keypair. This should be kept separate from encryptions
     * and decryptions when benchmarking, because RSA key generation can be slow.
     * @param bits - number of bits in the RSA key (modulus)
     * @return the new KeyPair
     */
    public static KeyPair genRSAKey(int bits) {
        KeyPairGenerator kpg = null;
        try {
            RSAKeyGenParameterSpec kgspec = new RSAKeyGenParameterSpec(bits, RSAKeyGenParameterSpec.F4);
            kpg = KeyPairGenerator.getInstance("RSA");
            kpg.initialize(kgspec);
        } catch (NoSuchAlgorithmException | InvalidAlgorithmParameterException ex) {
            Logger.getLogger(PKBenchmark.class.getName()).log(Level.SEVERE, null, ex);
        }
        return kpg.genKeyPair();
    }
    
    /**
     * Test RSA encryption speed. This should repeatedly do RSA encryptions,
     * using the supplied public key, as many times as are necessary so that it
     * runs approximately 10 seconds (so you get accurate timings).
     * 
     * NOTE: The plaintext size for a 2048-bit RSA key should be 245 bytes
     * 
     * 
     * @param pk
     * @return ciphertext (return so it doesn't optimize away the computation!)
     */
    public static byte[] testRSAEncrSpeed(PublicKey pk) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException {
        // YOUR CODE GOES HERE
        final int NUMTESTS=70000;
        String plaintext="000000000000000000000000000000000000000000000000"
                + "00000000000000000000000000000000000000000000000"
                + "0000000000000000000000000000000000000000000000000000"
                + "00000000000000000000000000000000000000000000000000000000000"
                + "000000000000000000000000000000000000000";

        Cipher c= Cipher.getInstance("RSA");
        c.init(Cipher.ENCRYPT_MODE,pk);
        for(int i=0;i<NUMTESTS;i++){
            c.doFinal(plaintext.getBytes());
        }
        //SealedObject myEncryptedMessage=new SealedObject(myMessage, c);

        return null;
    }

    /**
     * This function simply computes a valid RSA ciphertext. It really doesn't
     * matter what is encrypted, so this can just encrypt a byte array of all
     * zeros.
     *
     * NOTE: The plaintext size for a 2048-bit RSA key should be 245 bytes
     * 
     * @param pk the public key to use for encryption
     * @return the ciphertext
     */
    public static byte[] genRSACiphertext(PublicKey pk) throws InvalidKeyException, IllegalBlockSizeException, BadPaddingException, NoSuchAlgorithmException, NoSuchPaddingException {
         // YOUR CODE GOES HERE
        String plaintext="000000000000000000000000000000000000000000000000"
                + "00000000000000000000000000000000000000000000000"
                + "0000000000000000000000000000000000000000000000000000"
                + "00000000000000000000000000000000000000000000000000000000000"
                + "000000000000000000000000000000000000000";

        Cipher c= Cipher.getInstance("RSA");
        c.init(Cipher.ENCRYPT_MODE,pk);
        return c.doFinal(plaintext.getBytes());

    }

    /**
     * Test RSA decryption speed. This should repeatedly do RSA decryptions,
     * using the supplied public key, as many times as are necessary so that it
     * runs approximately 10 seconds (so you get accurate timings). Note that
     * since RSA padding modes include some tests for proper decryption, you
     * must supply the decrypt method with a valid RSA ciphertext. This is
     * passed as a parameter so that you don't have to compute it in this
     * function (which would mess up your timings).
     * @param key the RSA private key to use for decryption
     * @param ciphertext a valid RSA ciphertext
     * @return ciphertext (return so it doesn't optimize away the computation!)
     */    
    public static byte[] testRSADecrSpeed(PrivateKey key, byte[] ciphertext) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {
        // YOUR CODE GOES HERE
        final int NUMTESTS=1500;
        
        Cipher c= Cipher.getInstance("RSA");
        c.init(Cipher.DECRYPT_MODE,key);
        for(int i=0;i<NUMTESTS;i++){
            c.doFinal(ciphertext);
        }
        return null;
    }

    /**
     * The main program - comment/uncomment the methods that you want to time
     * @param args the command line arguments
     */
    public static void main(String[] args) throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException, IOException, IllegalBlockSizeException, BadPaddingException, InvalidAlgorithmParameterException {
        KeyPair kp = genRSAKey(2048);
        testRSAEncrSpeed(kp.getPublic());
        testRSADecrSpeed(kp.getPrivate(), genRSACiphertext(kp.getPublic()));
          
        testDH(getBobPublic("DiffieHellman", B64_BOBPK_DH));
        testECDH(getBobPublic("EC", B64_BOBPK_ECDH));

        
    }
}
